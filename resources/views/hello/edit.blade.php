<!DOCTYPE html>
<html lang="en">
  <head>

  </head>
  <body>

    <div class="container">
        <h2>Edit User</h2>
        {!! Form::open(array('method'=>'patch','route'=>['user.update',$user->id])) !!}
        <div class="form-group">
            <label for="">username</label>
            <input type="text" name="username" value="{{$user->name}}" class="form-control">
            <div class = "form-group">
              <label for="">mail</label>
              <input type="text" name="usermail" value="{{$user->email}}" class="form-control">
            </div>
            <button type"submit" class="btn btn-primary">Update</button>
        </div>
        {!! Form::close() !!}
    </div> <!--end of .container -->

  </body>
</html>
