<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Documents</title>

  </head>
  <body>

    <div class="container">
      <h2>Index page</h2>

      @if($users->count())
        <table>
            @foreach($users as $user)
              <tr>
                  <td>{{$user->id}}</td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td><a href="/user/{{$user->id}}/edit" class="button">Edit</a></td>

                  <td>
                  {!! Form::open(['method' => 'DELETE','route' => ['user.destroy', $user->id],'style'=>'display:inline']) !!}
                  {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                  {!! Form::close() !!}</td>
              </tr>
            @endforeach
            <div class="row">

            </div>

        </table>
      @endif
    </div> <!--end of .container -->

  </body>
</html>
