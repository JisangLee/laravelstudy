<!DOCTYPE html>
<html lang="en">
  <head>

  </head>
  <body>

    <div class="container">
      <h2>Create User</h2>
      {!! Form::open(array('route'=>'user.store'  )) !!}
          {!! Form::label('name','이름:') !!}
          {!! Form::text('name',null,array('class' => 'form-control')) !!}
          <br/>
          {!! Form::label('email','이메일:') !!}
          {!! Form::text('email',null,array('class' => 'form-control')) !!}
          <br/>
          {!! Form::label('password','비밀번호') !!}
          {!! Form::text('password',null,array('class' => 'form-control')) !!}
          <br/>
          {!! Form::submit('Create User', array('class' => 'btn btn-success btn-lg btn-block','style'=>'margin-top:20px;')) !!}
      {!! Form::close() !!}
    </div> <!--end of .container -->

  </body>
</html>
