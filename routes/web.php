<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});
/*
Route::get('posts',function(){
	return 'Test Message';
});*/
//기본값이 1인 형태로..
/*
Route::get('posts/{postId?}',function($postId = 1){
  return 'Post ID : '. $postId;
})->name('posts'); //라우트 네이밍


// Route::get('test','ItemController');//콘트롤러담에@함수명
Route::get('test','ItemController@test')->name('test');

Route::get('posts/{postId}/comments/{commentId}',function($postId, $commentId){
  return 'postID : '.$postId . ', commentId : '. $commentId;
});

//그룹->묶어줌.1.미들웨어를 통해묶거나 2.컨트롤러 네임스페이스에 따라서  3.URL요청 Pass Prefix값에 따라
Route::group(['middleware'=>'auth'],function(){
  Route::get('dashboard',function(){
    //middleware('auth')=>특정레벨의 사람들만 접근가능토록
    return 'Dashboard';
  });

  Route::get('user/profile',function(){
    return 'User Profile';
  });
});

//namespace ->
Route::group(['namespace'=>'Admin'],function(){
//App\Http\Controllers\Admin
});
//prefix
Route::group(['prefix'=>'system'],function(){
  Route::get('show',function(){
    // /system/show 이렇게 요청이온경우..
  });
});
Route::get('itemtest','ItemtestController@itemid')->name('itemid');
*/
// Route::resource('item','ItemController');
Route::resource('user','UserController');
// Route::resource('post','PostController');
