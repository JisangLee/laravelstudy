<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Input;
use Redirect;
use App\User;
use App\Http\Controllers\Controller;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('user.index',compact('users'));
        //return view('itemview',compact('items','varall'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //fetch request
        // validate the data
          $user = User::create($request->all());
          /*
          $this->validate($request,array(
              'name' =>'required|max:255',
              'email'  =>'requitred',
              'password' => 'required|min:6'
            ));
          $user = new User;
          $user->name = $request->name;
          $user -> save();
          */
          return redirect()->route('user.show',$user->id);



        //store in the Database

        //redirect to another page

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::find($id);
        // return $user;
        return view('user.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
    $user= User::findOrFail($id);
    $user->name   = Input::get('name');
    $user->email  = Input::get('email');

    $user->save();

    return Redirect::route('user.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      //Form::open(['method'=>'delete','route'=>['user.destroy',$user->id]]);
        User::find($id)->delete();
        return redirect()->route('user.index')->with('success','user deleted successfully');
    }
}
